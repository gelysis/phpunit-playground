PHPUNIT Playground
==================

Copyright ©2022, Andreas Gerhards age@gelysis.net.  
All rights reserved. / Alle Rechte vorbehalten. / Tous droits réservés.  

# LICENSE
The phpunit-playground is open source and licensed as [BSD-3-Clause](http://opensource.org/licenses/BSD-3-Clause).  

Please consult `LICENSE.md` for further details.  

# LIZENZ
Das phpunit-playground ist Open Source und unter der [BSD-3-Klausel](http://opensource.org/licenses/BSD-3-Clause) lizensiert.  

Bitte lesen sie `LICENSE.md` für weitergehende Informationen.  

# SYSTEM REQUIREMENTS
Requires PHP 8.1 or later.  

# DESCRIPTION
_TODO: add content_  

# INSTALLATION
_TODO: still to be implemented_  
* Use packagist dependency: _TODO: still to be implemented_  

# USAGE
_TODO: add content_  

# QUESTIONS AND FEEDBACK
Please contact the author.  

# RELEASE INFORMATION
No release yet  
2022-07-08  

# UPDATES
Please see `CHANGELOG.md`.  
